const Partners = () => {
  return (
    <section class="section" id="partners">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="content-panel">
              <div class="marquee" id="mycrawler2">
                <div style={{ width: "100%" }}>
                  <img
                    src="assets/images/logo-dnb.jpg"
                    alt="D&B/Hoovers"
                    class="partners-logos img-responsive"
                  />
                  <img
                    src="assets/images/logo-wilson.jpg"
                    alt="Wilson Sonsini"
                    class="partners-logos img-responsive"
                  />
                  <img
                    src="assets/images/logo-ama.jpg"
                    alt="American Marketing Association"
                    class="partners-logos img-responsive"
                  />
                  <img
                    src="assets/images/logo-aws.jpg"
                    alt="Amazon Web Services"
                    class="partners-logos img-responsive"
                  />
                  <img
                    src="assets/images/logo-iso.jpg"
                    alt="ISO"
                    class="partners-logos img-responsive"
                  />
                  <img
                    src="assets/images/logo-j.jpg"
                    alt="Jump Crew"
                    class="partners-logos img-responsive"
                  />
                  <img
                    src="assets/images/logo-services-source.jpg"
                    alt="Service Source"
                    class="partners-logos img-responsive"
                  />
                  <img
                    src="assets/images/logo-firecracker.jpg"
                    alt="Firecracker"
                    class="partners-logos img-responsive"
                  />
                  <img
                    src="assets/images/logo-ism.jpg"
                    alt="The Institute for Supply Management® (ISM®)"
                    class="partners-logos img-responsive"
                  />
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Partners;
