const Footer = () => {
  return (
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-12 col-sm-12">
            <div class="footr-logo">
              <a href="index.html">
                <img
                  src="assets/images/logo.png"
                  width="1000"
                  height="169"
                  alt=""
                />
              </a>
            </div>
            <p class="address">Address goes here</p>
            <ul class="social">
              <li>
                <a href="#">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-lg-4 col-md-12 col-sm-12">
            <div class="menus">
              <h5>Other Links</h5>
              <ul>
                <li>
                  <a href="link.html">Link One</a>
                </li>
                <li>
                  <a href="link.html">Link One</a>
                </li>
                <li>
                  <a href="link.html">Link One</a>
                </li>
                <li>
                  <a href="link.html">Link One</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-lg-4 col-md-12 col-sm-12">
            <div class="menus">
              <h5>Navigations</h5>
              <ul>
                <li>
                  <a href="about-us.html">About Us</a>
                </li>
                <li class="">
                  <a href="b2b-news.html">B2B News</a>
                </li>
                <li class="">
                  <a href="opportunities.html">Opportunities</a>
                </li>
                <li class="">
                  <a href="e-learning.html"> E-learning</a>
                </li>
                <li class="">
                  <a href="networking.html"> Networking</a>
                </li>
                <li class="">
                  <a href="tips-discussions.html"> Tips & Discussions </a>
                </li>
                <li class="">
                  <a href="partners.html"> Partners</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="foot-bottom">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <p class="copyright">Copyright &copy; 2021 Biz Connect</p>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};
export default Footer;
