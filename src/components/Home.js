import Header from "./Header";
import Footer from "./Footer";
import Partners from "./Partners";
import PropTypes from "prop-types";

const Home = ({ history }) => {
  return (
    <>
      <Header></Header>
      <div class="welcome-area" id="welcome">
        <div class="welcome-bg">
          <div class="header-text">
            <div class="container">
              <div class="row">
                <div
                  class="left-text col-lg-8 col-md-6 col-sm-12 col-xs-12"
                  data-scroll-reveal="enter left move 30px over 0.6s after 0.4s"
                >
                  <h1>A Global B2B Search Engine And Community</h1>
                  <a href="#" class="main-button-slider">
                    Read More
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <section class="feature" id="features">
        <div class="container">
          <div class="row">
            <div class="center-text">
              <h1>Our Offerings</h1>
              <h5>Our marketing plan reaches tens of millions of B2B</h5>
            </div>
            <div class="owl-carousel owl-theme">
              <div class="item feature-item">
                <div class="icon">
                  <i>
                    <img src="assets/images/service-icon-01.png" alt="" />
                  </i>
                </div>
                <h5 class="feature-title">Global Business Search</h5>
                <p>
                  Aenean vulputate massa sed neque consectetur, ac fringilla
                  quam aliquet. Sed a enim nec eros tempor cursus at id libero.
                </p>
              </div>
              <div class="item feature-item">
                <div class="icon">
                  <i>
                    <img src="assets/images/service-icon-02.png" alt="" />
                  </i>
                </div>
                <h5 class="feature-title">Business Profile</h5>
                <p>
                  Pellentesque vitae urna ut nisi viverra tristique quis at
                  dolor. In non sodales dolor, id egestas quam. Aliquam erat
                  volutpat.{" "}
                </p>
              </div>

              <div class="item feature-item">
                <div class="icon">
                  <i>
                    <img src="assets/images/service-icon-01.png" alt="" />
                  </i>
                </div>
                <h5 class="feature-title">Global Business Outreach</h5>
                <p>
                  Curabitur aliquam eget tellus id porta. Proin justo sapien,
                  posuere suscipit tortor in, fermentum mattis elit.
                </p>
              </div>

              <div class="item feature-item">
                <div class="icon">
                  <i>
                    <img src="assets/images/service-icon-03.png" alt="" />
                  </i>
                </div>
                <h5 class="feature-title">Chat Facility</h5>
                <p>
                  Quisque finibus libero augue, in ultrices quam dictum id.
                  Aliquam quis tellus sit amet urna tincidunt bibendum.
                </p>
              </div>
              <div class="item feature-item">
                <div class="icon">
                  <i>
                    <img src="assets/images/service-icon-02.png" alt="" />
                  </i>
                </div>
                <h5 class="feature-title">Multicurrency Payment</h5>
                <p>
                  Fusce sollicitudin feugiat risus, tempus faucibus arcu blandit
                  nec. Duis auctor dolor eu scelerisque vestibulum.
                </p>
              </div>
              <div class="item feature-item">
                <div class="icon">
                  <i>
                    <img src="assets/images/service-icon-01.png" alt="" />
                  </i>
                </div>
                <h5 class="feature-title">Pay Per Click</h5>
                <p>
                  Curabitur aliquam eget tellus id porta. Proin justo sapien,
                  posuere suscipit tortor in, fermentum mattis elit.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="section" id="about">
        <div class="container">
          <div class="row">
            <div
              class="col-lg-6 col-md-12 col-sm-12"
              data-scroll-reveal="enter left move 30px over 0.6s after 0.4s"
            >
              <img
                src="assets/images/left-image.png"
                class="rounded img-fluid d-block mx-auto"
                alt=""
              />
            </div>
            <div class="right-text col-lg-6 col-md-12 col-sm-12 mobile-top-fix">
              <div class="left-heading">
                <h5>Why BizConnect?</h5>
              </div>
              <div class="left-text">
                <p>
                  The global B2B E-Commerce is a multi-trillion-dollar industry
                  that is growing rapidly. There are many players in the
                  industry that have created product marketplaces that serve
                  regions and industries.
                </p>
                <p>
                  Bizconnect has created the technology, team, and partnerships
                  to be the first to market an exclusive global B2B search
                  engine and hub with tens of millions of businesses categorized
                  by keyword. Through the partnership with many companies,
                  Bizconnect has created a verified environment of buyers and
                  sellers.
                </p>
                <p style={{ marginBottom: "45px" }}>
                  The Company has developed a lengthy list of first-mover
                  advantages to maintain a leadership position in the global B2B
                  search engine market.
                </p>
                <a href="#" class="main-button">
                  Discover More
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="section" id="services">
        <div class="container">
          <div class="row">
            <div class="center-text">
              <h1>What BizConnect Offers?</h1>
              <h5>
                Our marketing plan will reach the tens of millions of B2B buyers
                and advertisers through a variety of marketing methods.
              </h5>
              <button type="button" class="main-button">
                Get Started
              </button>
            </div>
            <div class="owl-carousel owl-theme">
              <div class="item service-item">
                <div class="icon">
                  <i>
                    <img src="assets/images/service-icon-01.png" alt="" />
                  </i>
                </div>
                <h5 class="service-title">First Box Service</h5>
                <p>
                  Aenean vulputate massa sed neque consectetur, ac fringilla
                  quam aliquet. Sed a enim nec eros tempor cursus at id libero.
                </p>
                <a href="#" class="main-button">
                  Read More
                </a>
              </div>
              <div class="item service-item">
                <div class="icon">
                  <i>
                    <img src="assets/images/service-icon-02.png" alt="" />
                  </i>
                </div>
                <h5 class="service-title">Second Box Title</h5>
                <p>
                  Pellentesque vitae urna ut nisi viverra tristique quis at
                  dolor. In non sodales dolor, id egestas quam. Aliquam erat
                  volutpat.{" "}
                </p>
                <a href="#" class="main-button">
                  Discover More
                </a>
              </div>
              <div class="item service-item">
                <div class="icon">
                  <i>
                    <img src="assets/images/service-icon-03.png" alt="" />
                  </i>
                </div>
                <h5 class="service-title">Third Title Box</h5>
                <p>
                  Quisque finibus libero augue, in ultrices quam dictum id.
                  Aliquam quis tellus sit amet urna tincidunt bibendum.
                </p>
                <a href="#" class="main-button">
                  More Detail
                </a>
              </div>
              <div class="item service-item">
                <div class="icon">
                  <i>
                    <img src="assets/images/service-icon-02.png" alt="" />
                  </i>
                </div>
                <h5 class="service-title">Fourth Service Box</h5>
                <p>
                  Fusce sollicitudin feugiat risus, tempus faucibus arcu blandit
                  nec. Duis auctor dolor eu scelerisque vestibulum.
                </p>
                <a href="#" class="main-button">
                  Read More
                </a>
              </div>
              <div class="item service-item">
                <div class="icon">
                  <i>
                    <img src="assets/images/service-icon-01.png" alt="" />
                  </i>
                </div>
                <h5 class="service-title">Fifth Service Title</h5>
                <p>
                  Curabitur aliquam eget tellus id porta. Proin justo sapien,
                  posuere suscipit tortor in, fermentum mattis elit.
                </p>
                <a href="#" class="main-button">
                  Discover
                </a>
              </div>
              <div class="item service-item">
                <div class="icon">
                  <i>
                    <img src="assets/images/service-icon-03.png" alt="" />
                  </i>
                </div>
                <h5 class="service-title">Sixth Box Title</h5>
                <p>
                  Ut nibh velit, aliquam vitae pellentesque nec, convallis vitae
                  lacus. Aliquam porttitor urna ut pellentesque.
                </p>
                <a href="#" class="main-button">
                  Detail
                </a>
              </div>
              <div class="item service-item">
                <div class="icon">
                  <i>
                    <img src="assets/images/service-icon-01.png" alt="" />
                  </i>
                </div>
                <h5 class="service-title">Seventh Title Box</h5>
                <p>
                  Sed a consequat velit. Morbi lectus sapien, vestibulum et
                  sapien sit amet, ultrices malesuada odio. Donec non quam.
                </p>
                <a href="#" class="main-button">
                  Read More
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Partners></Partners>
      <Footer></Footer>
    </>
  );
};

Home.propTypes = {
  history: PropTypes.object,
};
export default Home;
